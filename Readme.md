zcodec
======

Usage
------------

```scala
    import zcodec._
    import decoders._


    case class Point(int0: Int, int1: Int, s0: String, s1: String)

    implicit val delimiter = Delimiter("&")

    val hdecoder = (int :: int :: string :: string).as[Point]

    hdecoder.decode("14&88&ahgsjdh&12u3uigjksf&asjdasld")
```
