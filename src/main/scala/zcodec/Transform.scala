package zcodec

import cats.data.Xor

abstract class Transform[F[_]] { self =>

  /**
    * Transforms supplied `F[A]` to an `F[B]` using two functions, `A => Attempt[B]` and `B => Attempt[A]`.
    */
  def exmap[A, B](fa: F[A], f: A => Xor[Throwable, B]): F[B]

  /**
    * Transforms supplied `F[A]` to an `F[B]` using the isomorphism described by two functions,
    * `A => B` and `B => A`.
    */
  def xmap[A, B](fa: F[A], f: A => B): F[B] =
    exmap[A, B](fa, a => Xor.right(f(a)))

  def as[A, B](fa: F[A])(implicit as: Transformer[A, B]): F[B] = as(fa)(self)
}

/** Companion for [[Transform]]. */
object Transform {
  def apply[F[_]](implicit t: Transform[F]): Transform[F] = t
}
