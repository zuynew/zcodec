package zcodec

import shapeless._

abstract class Transformer[A, B] {
  def apply[F[_]: Transform](fa: F[A]): F[B]
}

/** Companion for [[Transformer]]. */
object Transformer {
  implicit def id[A]: Transformer[A, A] = new Transformer[A, A] {
    def apply[F[_]: Transform](fa: F[A]): F[A] = fa
  }

  /** Builds a `Transformer[A, B]` from a Shapeless `Generic.Aux[B, A]`. */
  implicit def fromGenericReverse[A, B](
      implicit gen: Generic.Aux[B, A]): Transformer[A, B] =
    new Transformer[A, B] {
      def apply[F[_]: Transform](fa: F[A]): F[B] =
        fa.xmap(a => gen.from(a))
    }
}
