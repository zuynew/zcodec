package zcodec

case class DecodeResult[+A](value: A, remainder: String) {

  def map[B](f: A => B): DecodeResult[B] = DecodeResult(f(value), remainder)

}
