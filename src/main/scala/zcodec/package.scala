import shapeless._
import zcodec.decoders.HListDecoder

package object zcodec {

  implicit class HListCodecEnrichedWithHListSupport[L <: HList](
      val self: Decoder[L]) {

    import HListDecoder._

    def ::[B](codec: Decoder[B]): Decoder[B :: L] =
      HListDecoder.prepend(codec, self)

  }

  implicit class ValueCodecEnrichedWithHListSupport[A](val self: Decoder[A]) {

    import HListDecoder._

    def ::[B](codecB: Decoder[B]): Decoder[B :: A :: HNil] =
      codecB :: self :: HListDecoder.hnilDecoder
  }

  implicit class TransformSyntax[F[_], A](val self: F[A])(implicit t: Transform[F]) {

    def xmap[B](f: A => B): F[B] = t.xmap(self, f)

    def as[B](implicit as: Transformer[A, B]): F[B] = as(self)
  }

}
