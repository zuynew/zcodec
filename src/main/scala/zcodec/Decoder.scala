package zcodec

import cats.data.Xor

trait Decoder[+A] { self =>

  def decode(buffer: String)(
      implicit delimiter: Delimiter): Xor[Throwable, DecodeResult[A]]

  def map[B](f: A => B): Decoder[B] = new Decoder[B] {
    def decode(string: String)(implicit delimiter: Delimiter) =
      self.decode(string) map { _ map f }
  }
  def emap[B](f: A => Xor[Throwable, B]): Decoder[B] = new Decoder[B] {
    def decode(string: String)(implicit delimiter: Delimiter) = {
      for {
        a <- self.decode(string)
        b <- f(a.value)
      } yield DecodeResult(b, a.remainder)
    }
  }
}

trait DecoderFunctions {
  def decodeBothCombine[A, B, C](decA: Decoder[A], decB: Decoder[B])(
      buffer: String)(f: (A, B) => C)(
      implicit delimiter: Delimiter): Xor[Throwable, DecodeResult[C]] = {
    for {
      a <- decA.decode(buffer)
      b <- decB.decode(a.remainder)
    } yield DecodeResult(f(a.value, b.value), b.remainder)
  }
}

object Decoder extends DecoderFunctions {

  def split(string: String)(implicit delimiter: Delimiter) =
    string.split("\\" + delimiter.delimiter)

  implicit val transformInstance: Transform[Decoder] = new Transform[Decoder] {
    def exmap[A, B](decoder: Decoder[A],
                    f: A => Xor[Throwable, B]): Decoder[B] =
      decoder.emap(f)

    override def xmap[A, B](decoder: Decoder[A], f: A => B): Decoder[B] =
      decoder.map(f)
  }

}
