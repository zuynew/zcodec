package zcodec.decoders

sealed trait DecodingException
case class EmptyBufferParsingException() extends Exception("Fail to parse empty buffer")
