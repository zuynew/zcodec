package zcodec.decoders

import cats.data.Xor
import shapeless._
import zcodec.{DecodeResult, Decoder, Delimiter}

object HListDecoder {

  val hnilDecoder: Decoder[HNil] = new Decoder[HNil] {
    override def decode(string: String)(
        implicit delimiter: Delimiter): Xor[Throwable, DecodeResult[HNil]] =
      Xor.right(DecodeResult(HNil, string))
  }

  def prepend[A, L <: HList](a: Decoder[A], l: Decoder[L]): Decoder[A :: L] =
    new Decoder[A :: L] {
      override def decode(buffer: String)(implicit delimiter: Delimiter)
        : Xor[Throwable, DecodeResult[::[A, L]]] =
        Decoder.decodeBothCombine(a, l)(buffer) { _ :: _ }
    }
}
