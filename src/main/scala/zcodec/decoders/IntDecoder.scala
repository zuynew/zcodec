package zcodec.decoders

import cats.data.Xor
import zcodec.Decoder._
import zcodec.{DecodeResult, Decoder, Delimiter}

class IntDecoder() extends Decoder[Int] {
  override def decode(buffer: String)(
      implicit delimiter: Delimiter): Xor[Throwable, DecodeResult[Int]] = {
    val splitted = split(buffer)
    for {
      head <- Xor.fromOption(splitted.headOption, EmptyBufferParsingException)
      intValue <- Xor.catchOnly[NumberFormatException](head.toInt)
    } yield
      DecodeResult[Int](intValue, splitted.tail.mkString(delimiter.delimiter))
  }
}
