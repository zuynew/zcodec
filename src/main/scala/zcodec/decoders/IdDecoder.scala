package zcodec.decoders

import cats.data.Xor
import zcodec.Decoder._
import zcodec.{DecodeResult, Decoder, Delimiter}

class IdDecoder() extends Decoder[String] {
  override def decode(buffer: String)(
      implicit delimiter: Delimiter): Xor[Throwable, DecodeResult[String]] = {
    val splitted = split(buffer)
    Xor.fromOption(splitted.headOption, EmptyBufferParsingException) map {
      stringValue =>
        DecodeResult[String](stringValue,
                             splitted.tail.mkString(delimiter.delimiter))
    }
  }
}
