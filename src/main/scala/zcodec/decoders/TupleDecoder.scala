package zcodec.decoders

import cats.data.Xor
import zcodec.{DecodeResult, Decoder, Delimiter}

class TupleDecoder[A, B](decoderA: Decoder[A], decoderB: Decoder[B])
    extends Decoder[(A, B)] {
  override def decode(buffer: String)(
      implicit delimiter: Delimiter): Xor[Throwable, DecodeResult[(A, B)]] = {
    for {
      a <- decoderA.decode(buffer)
      b <- decoderB.decode(a.remainder)
    } yield DecodeResult((a.value, b.value), b.remainder)
  }
}
