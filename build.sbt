name := "codec"

version := "0.1"

scalaVersion := "2.11.5"


libraryDependencies += "com.chuusai" %% "shapeless" % "2.3.2"

libraryDependencies += "org.typelevel" %% "cats" % "0.6.1"

